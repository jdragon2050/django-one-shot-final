from django.contrib import admin
from django.urls import path, include
from .views import show_todos, show_todo, create_todo, todo_list_update, todo_list_delete, create_todo_item, todo_item_update, todo_item_delete, todo_item_search

urlpatterns = [
    path('todos/', show_todos, name='todo_list_list'),
    path('todos/<int:todo_id>/', show_todo, name='todo_list_detail'),
    path('todos/create/', create_todo, name='todo_list_create'),
    path('todos/<int:todo_id>/edit/', todo_list_update, name='todo_list_update'),
    path('todos/<int:todo_id>/delete/', todo_list_delete, name='todo_list_delete'),
    path('todos/items/create/', create_todo_item, name='todo_item_create'),
    path('todos/items/<int:todo_id>/edit/', todo_item_update, name='todo_item_update'),
    path('todos/items/<int:todo_id>/delete/', todo_item_delete, name='todo_item_delete'),
    path('todos/items/search/', todo_item_search, name='todo_item_search'),
]