from django.shortcuts import render, redirect
from .models import TodoList, TodoItem
from datetime import datetime
from django.utils import timezone


# Create your views here.
def show_todos(request):
  todos = TodoList.objects.all()
  return render(request, 'todos/todos.html', {'todos': todos})

def show_todo(request, todo_id):
    todo = TodoList.objects.get(id=todo_id)

    upcoming_tasks = todo.items.filter(due_date__gte=timezone.now()).order_by('due_date')
    completed_tasks = todo.items.filter(is_completed=True)

    return render(request, 'todos/todo.html', {'todo_item': todo, 'upcoming_tasks': upcoming_tasks, 'completed_tasks': completed_tasks})


def create_todo(request):
  if request.method == 'POST':
    name = request.POST['name']
    new_todo = TodoList(name=name)
    new_todo.save()
    return redirect(f'/todos/{new_todo.id}/')
  else:
    return render(request, 'todos/create_todo.html')

def todo_list_update(request, todo_id):
    todo = TodoList.objects.get(id=todo_id)
    if request.method == 'POST':
        name = request.POST['name']
        todo.name = name
        todo.save()
        return redirect(f'/todos/{todo.id}/')
    else:
        return render(request, 'todos/edit_todo.html', {'todo': todo})
  
def todo_list_delete(request, todo_id):
   todo = TodoList.objects.get(id=todo_id)
   if request.method == 'POST':
      todo.delete()
      return redirect('/todos/')
   else:
      return render(request, 'todos/delete_todo.html', {'todo': todo})

def create_todo_item(request):
    todos = TodoList.objects.all()
    context = {'todos': todos}

    if request.method == 'POST':
        task = request.POST.get('task') 
        due_date = request.POST.get('due_date')
        print('DUE DATE VALUE', due_date)
        if due_date:
            new_due_date = datetime.strptime(due_date, "%m/%d/%Y")
        else:
            new_due_date = None
        is_completed = request.POST.get('is_completed') == 'on'  
        list_id = request.POST.get('list') 
        
        if task:
            new_todo_item = TodoItem(task=task, due_date=new_due_date, is_completed=is_completed, list_id=list_id)
            new_todo_item.save()
            return redirect(f'/todos/{list_id}/')  
        else:
            pass
    else:
        return render(request, 'todos/create_todo_item.html', context)

def todo_item_update(request, todo_id):
    todo_item = TodoItem.objects.get(id=todo_id)
    todos = TodoList.objects.all()
    context = {'todo_item': todo_item, 'todos': todos}
    if request.method == 'POST':
        task = request.POST.get('task') 
        due_date = request.POST.get('due_date')
        print('DUE DATE VALUE', due_date)
        if due_date:
            new_due_date = datetime.strptime(due_date, "%m/%d/%Y")
        else:
            new_due_date = None



        is_completed = request.POST.get('is_completed') == 'on'  
        list_id = request.POST.get('list') 
        
        if task:
            todo_item.task = task
            todo_item.due_date = new_due_date
            todo_item.is_completed = is_completed
            todo_item.list_id = list_id
            todo_item.save()
            return redirect(f'/todos/{list_id}/')  
        else:
            return render(request, 'todos/todo_item_update.html', context)
    return render(request, 'todos/todo_item_update.html', context)

def todo_item_delete(request, todo_id):
    todo_item = TodoItem.objects.get(id=todo_id)
    list_id = todo_item.list.id 

    if request.method == 'POST':
        todo_item.delete()
        return redirect(f'/todos/{list_id}/')  
    else:
        return render(request, 'todos/todo_item_delete.html', {'todo_item': todo_item})

def todo_item_search(request):
    query = request.GET.get('q')
    category = request.GET.get('category')
    
    results = TodoItem.objects.all()
    if query:
        results = results.filter(task__icontains=query)
    if category:
        results = results.filter(category=category)
        
    return render(request, 'todos/todo_item_search.html', {'results': results, 'query': query, 'category': category})